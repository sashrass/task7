protocol FamilyMemberProtocol{
    var name: String {get set}
}

class FamilyMember: FamilyMemberProtocol{
    var name: String
    
    init(name: String){
        self.name = name
    }
}

class Child: FamilyMember{
    weak var father: Father!
    weak var mother: Mother!
    weak var otherChild: Child!
    
    lazy var askMother: () -> Void = {
        [unowned self] in
        mother.giveCandy(childName: self.name)
    }
    
    lazy var askFather: () -> Void = {
        [unowned self] in
        father.buyToy(name: self.name)
    }
    
    lazy var askAnotherChild: () -> Void = {
        [unowned self] in
        otherChild.giveToy(name: self.name)
    }
    
    func giveToy(name: String){
        print("Держи игрушку, \(name)")
    }
    
    deinit{
        print("\(self.name) уничтожен")
    }
}

class Mother: FamilyMember{
    weak var firstChild: Child!
    weak var secondChild: Child!
    
    func giveCandy(childName: String){
        print("Держи конфету, \(childName)")
    }
    
    deinit{
        print("\(self.name) уничтожен")
    }
}

class Father: FamilyMember{
    weak var firstChild: Child!
    weak var secondChild: Child!
    weak var wife: Mother!
    weak var family: Family!
    
    func buyToy(name: String){
        print("Игрушка куплена, \(name)")
    }
    
    lazy var printFamily: () -> Void = {
        [unowned self] in
        for familyMember in self.family.getAllFamilyMembers(){
            print(familyMember.name)
        }
    }
    
    lazy var printWife: () -> Void = {
        [unowned self] in
        print(self.wife.name)
    }
    
    lazy var printChildren: () -> Void = {
        [unowned self] in
        print("\(firstChild.name)\n\(secondChild.name)")
    }
    
    deinit{
        print("\(self.name) уничтожен")
    }
}

class Family{
    var father: Father
    var mother: Mother
    var firstChild: Child
    var secondChild: Child
    
    func getAllFamilyMembers() -> [FamilyMemberProtocol]{
        return [father, mother, firstChild, secondChild]
    }
    
    init(father: Father, mother: Mother, firstChild: Child, secondChild: Child){
        self.father = father
        self.mother = mother
        self.firstChild = firstChild
        self.secondChild = secondChild
        
        self.father.wife = mother
        self.father.firstChild = firstChild
        self.father.secondChild = secondChild
        self.father.family = self
        
        self.mother.firstChild = firstChild
        self.mother.secondChild = secondChild
        
        self.firstChild.mother = mother
        self.firstChild.father = father
        self.firstChild.otherChild = secondChild
        
        self.secondChild.mother = mother
        self.secondChild.father = father
        self.secondChild.otherChild = firstChild
    }
    
    deinit{
        print("Объект 'Family' уничтожен")
    }
}


var family: Family! = Family(father: Father(name: "Bob"), mother: Mother(name: "Kate"), firstChild: Child(name: "John"), secondChild: Child(name: "Jame"))

print("Состав семьи")
for member in family.getAllFamilyMembers(){
    print(member.name)
}
print("Состав семьи (вызов из объекта Father):")
family.father.printFamily()
print("Объект mother (вызов из объекта Father):")
family.father.printWife()
print("Объекты Child (вызов из объекта Father):")
family.father.printChildren()
print("Вызов buyToy объекта Father из объекта Child:")
family.firstChild.askFather()
print("Вызов giveCandy объекта Mother из объекта Child:")
family.firstChild.askMother()
print("Вызов giveToy объекта Child из другого объекта Child:")
family.firstChild.askAnotherChild()
family = nil
